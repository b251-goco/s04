# The goal of the activity is to be able to create a simple application that applies the four tenets of Object-Oriented Programming in the context of Python.

# 1. Create an abstract class called Animal that has the following abstract methods:
from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
# 	a. eat(food)
	def eat(self, food):
		pass
# 	b. make_sound()
	def make_sound(self):
		pass
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
class Dog(Animal):
# a. Properties:
# 	i. Name
# 	ii. Breed
# 	iii. Age
	def __init__(self, name, breed, age, sound):
		self._name = name
		self._breed = breed
		self._age = age
		self._sound = sound
# b. Methods:
# 	i. Getters
	def get_name(self):
		print(f"Hi, this is {self._name}")
	def get_breed(self):
		print(f"It is a {self._breed}")
	def get_age(self):
		print(f"{self._name} is {self._age}")
	def get_sound(self):
		self.make_sound()
# 	ii. Setters
	def set_name(self, name):
		self._name = name
	def set_breed(self, breed):
		self._breed = breed
	def set_age(self, age):
		self._age = age
	def set_sound(self, sound):
		self._sound = sound
# 	iii. Implementation of abstract methods
	def make_sound(self):
		print(f"\"{self._sound}\", {self._name} said")
	def eat(self, food):
		print(f"{self._name} ate the {food}")
# 	iv. call()
	def call(self):
		print(f"Come here, {self._name}")

class Cat(Dog):
	def __init__(self, name, breed, age, sound):
		super().__init__(name, breed, age, sound)

dog1 = Dog("Gorou", "Pitbull", 3, "Bork!")
dog1.eat("Hotdog")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Tabby", 4, "En garde!")
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

print("My pets are ... ")
for pets in (cat1, dog1):
	print(f"a {pets._breed} named {pets._name}")

